using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GuideEvent : Singleton<GuideEvent>
{
    //储存所有新手教程信息的字典<id,GuideInfo>
    private Dictionary<int, GuideInfo> guideInfoDict = new Dictionary<int, GuideInfo>();
    public void Init()
    {
        gameObject.name = "GuideEvent";
    }

    public void AddGuideInfoDict(int id, GuideInfo guideInfo, Transform trans)
    {
        if (guideInfoDict.ContainsKey(id))
        {
            Debug.LogError(string.Format("{0}的guideID-{1}重复,物体位于{2}", trans.name, id, trans.parent.name));
            return;
        }
        guideInfoDict.Add(id, guideInfo);
    }

    public GuideInfo GetGuideInfo(int id)
    {
        if (id != 0)
        {
            if (guideInfoDict.ContainsKey(id))
            {
                Debug.Log(string.Format("有这个教程 - {0}", id));
                return guideInfoDict[id];
            }
        }
        else
        {
            Debug.LogError(string.Format("没有这个教程 - {0}", id));
        }
        return new GuideInfo(id, null);
    }

    public bool IsHaveGuideInfo(int id)
    {
        return guideInfoDict.ContainsKey(id);
    }

    #region 游戏教程显示检查
    //第一次进入游戏
    public void FirstEnterUIGuideCheck()
    {
        if (PlayerData.instance.firstEnterGame == false)
        {
            GuidePanel.instance.ShowGuideById(101);
            return;
        }
    }
    /// <summary>
    /// 第一次升级
    /// </summary>
    public void FirstUpgradeGuideCheck()
    {
        if (PlayerData.instance.firstUpgrade == false)
        {
            GuidePanel.instance.ShowGuideById(201);
        }
    }

    #endregion

    #region Event

    #region 第一次进入游戏教程
    public void FirstEnterUIGuideOver()
    { 
        PlayerData.instance.firstEnterGame = true;
        Debug.Log("触发事件 FirstEnterUIGuideOver");
    }

    public void ShowArrow()
    {
        GuidePanel.instance.SetArrowState(true);
    }

    public void HideArrow()
    {
        GuidePanel.instance.SetArrowState(false);
    }
    #endregion

    #region 第一次升级教程
    public void EnterPropertyPanel()
    {
//         UIManager.instance.ShowPanel(PanelType.PropertyPanel);
//         PropertyPanel.Instance.ChangeTab(0);
//         PropertyPanel.Instance.toggles[0].isOn = true;
    }

    public void FirstUpgradeGuideOver()
    {
        //PlayerData.Instance.playerData.guideInfo.firstUpgrade = true;
    }
    #endregion
    #endregion
}

public class GuideInfo
{
    public int guideID;
    public RectTransform trans;
    public string guideText;
    public Sprite roleSprite;
    public string startEvent;
    public string clickEvent;
    public GuideShowType showType;
    public MaskType maskType;
    public bool isShowBG;
    public int nextID;
    public DialogDir dialogDir;

    //在构造函数中将新手教程所需要的信息都加载进来
    public GuideInfo(int guideID, RectTransform trans)
    {
        this.guideID = guideID;
        this.trans = trans;
        this.guideText = GuideTextData.instance.GetText(guideID);
        if (!string.IsNullOrEmpty(GuideTextData.instance.GetRoleSprite(guideID)))
        {
            this.roleSprite = null;// AtlasManager.Instance.GetSprite(AtlasType.UIAtlas, GuideTextData.Instance.GetRoleSprite(guideID));
        }
        this.startEvent = GuideTextData.instance.GetEvent(guideID);
        this.clickEvent = GuideTextData.instance.GetClickAction(guideID);
        this.showType = GuideTextData.instance.GetShowType(guideID);
        this.maskType = GuideTextData.instance.GetMaskType(guideID);
        this.isShowBG = GuideTextData.instance.GetIsShowBG(guideID);
        this.dialogDir = GuideTextData.instance.GetDialogDir(guideID);
    }
}

public enum ClickType
{
    FullScreen,
    Button,
}


public enum GuideShowType
{
    None,
    Clone,
    Mask
}

public enum MaskType
{
    None,
    Circle,
    Rect,
}

public enum DialogDir
{
    Left,
    Right,
    Top,
    Bottom,
    Mid
}
