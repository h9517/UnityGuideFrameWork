using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : Singleton<UIManager>
{
    public List<GameObject> UIPanels;
    public GameObject guidePanel;

    private void Start()
    {
        GuideEvent.instance.FirstEnterUIGuideCheck();
    }

    public void ShowGuidePanel(bool flag)
    {
        if (guidePanel.activeSelf != flag) 
            guidePanel.SetActive(flag);
    }
}
