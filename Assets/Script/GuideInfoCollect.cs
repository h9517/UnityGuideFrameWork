using UnityEngine;
using UnityEngine.UI;

public class GuideInfoCollect : MonoBehaviour
{
    public int guideID;
    private GuideInfo guideInfo;
    //public UIAnimDir dir = UIAnimDir.None;//ui动画位置（动态生成的才需要选择）
    [Header("新手教程数据记录结束后此物体active状态")]
    public bool setActive = true;
    void Start()
    {
        if (!GuideEvent.instance.IsHaveGuideInfo(guideID))
        {
            SetGuideInfo();

            //自动添加点击进行下一步引导的监听
            Button btn = GetComponent<Button>();
            if (btn != null)
            {
                btn.onClick.AddListener(ClickNext);
            }
        }
    }

    /// <summary>
    /// 在初始化的时候将所有新手教程的信息储存起来
    /// </summary>
    public void SetGuideInfo()
    {
        //Debug.Log(string.Format("名字-{0}，坐标{1}", transform.name, transform.position));
        if (guideID == 0)
        {
            Debug.LogError(string.Format("{0}的guideId为0,请在面板上赋值，物体位于{1}", this.gameObject.name, this.transform.parent.name));
            return;
        }
        else
        {
            //Debug.Log(string.Format("当前的guiID{0},物体名字是{1}", guideID, this.gameObject.name));
            guideInfo = GuideTextData.instance.GetGuideInfo(guideID, this.transform.GetComponent<RectTransform>());

            if (guideInfo != null)
            {
                GuideEvent.instance.AddGuideInfoDict(guideID, guideInfo, this.transform);
            }
        }

        //记录完移除此脚本
        //BoxCollider boxCollider = transform.GetComponent<BoxCollider>();
        //if (gameObject.name.CompareTo("GuideBox") == 0)
        //{
        //    Destroy(this.gameObject);
        //}
        //else
        //{
        //    gameObject.SetActive(setActive);
        //    Destroy(this);
        //}
    }

    private void ClickNext()
    {
        GuidePanel.instance.ClickNext();

        if (gameObject.name.Equals("GuideBox"))
        {//自行创建的引导区域
            Destroy(this.gameObject);
        }
        else
        {
            gameObject.SetActive(setActive);
            Destroy(this);
        }
    }

}