using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerData : Singleton<PlayerData>
{
    public int guideId;
    public bool firstEnterGame;
    public bool firstUpgrade;
}
