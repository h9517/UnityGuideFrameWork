using System;
using System.Collections.Generic;
using UnityEngine;

public class GuideTextData : Singleton<GuideTextData>
{
    Dictionary<int, GuideConfig> guideDic = new Dictionary<int, GuideConfig>();
    Dictionary<int,GuideConfig> GuideDic
    {
        get
        {
            if(guideDic.Count == 0)
            {
                string info = Resources.Load<TextAsset>("GuideConfig").ToString();
                Config config = JsonUtility.FromJson<Config>(info);
                List<GuideConfig> guideDatas = config.info;
                if (guideDatas == null)
                {
                    Debug.LogError("引导数据错误，检查配表 ！！！");
                }
                else
                {
                    foreach (GuideConfig item in guideDatas)
                    {
                        if (guideDic.ContainsKey(item.ID))
                        {
                            Debug.LogError("引导数据存在重复id，检查配表 ！！！");
                        }
                        else
                        {
                            guideDic.Add(item.ID, item);
                        }
                    }
                }
            }
            return guideDic;
        }
    }

    public GuideInfo GetGuideInfo(int guideID, RectTransform rect)
    {
        return new GuideInfo(guideID, rect);
    }

    private GuideConfig GetGuideJson(int guideID)
    {
        GuideConfig data;
        if (!GuideDic.TryGetValue(guideID, out data))
        {
            Debug.LogError("没有指定id的引导数据");
        }
        return data;
    }

    public string GetText(int guideID)
    {
        return GetGuideJson(guideID)?.GuideText;
    }

    public bool GetIsShowBG(int guideID)
    {
        return GetGuideJson(guideID)?.IsShowBG == 1;
    }

    public string GetRoleSprite(int guideID)
    {
        return GetGuideJson(guideID)?.RoleSprite;
    }

    public string GetEvent(int guideID)
    {
        return GetGuideJson(guideID)?.Event;
    }

    public string GetClickAction(int guideID)
    {
        return GetGuideJson(guideID)?.ClickAction;
    }

    public int GetNextID(int guideID)
    {
        return (int)GetGuideJson(guideID)?.NextID;
    }

    public GuideShowType GetShowType(int guideID)
    {
        return (GuideShowType)Enum.Parse(typeof(GuideShowType), GetGuideJson(guideID)?.ShowType);
    }

    public MaskType GetMaskType(int guideID)
    {
        return (MaskType)Enum.Parse(typeof(MaskType), GetGuideJson(guideID)?.MaskType);
    }

    public DialogDir GetDialogDir(int guideID)
    {
        return (DialogDir)Enum.Parse(typeof(DialogDir), GetGuideJson(guideID)?.DialogDir);
    }
}


public class Config
{
    public List<GuideConfig> info;
}

[Serializable]
public class GuideConfig
{
    public int ID;//引导id
    public int IsShowBG;//是否显示引导面板背景 0不显示 1显示
    public string GuideText;//教程描述文字
    public string DialogDir;//文本框位置
    public string RoleDir;//角色显示方位 没实现
    public string RoleSprite;//角色icon
    public string FingerType;//没实现
    public string Event;//引导显示前需要调用的事件
    public string ShowType;//分为Mask（遮罩模式） 和 Clone（克隆模式），主要区别在于克隆是将游戏中需要显示教程的地方克隆一份出来，放在ui的最上层，比较美观。Mask主要用遮罩镂空的方式实现教程。
    public string MaskType; //遮罩类型主要分为Circle(圆形遮罩)和Rect（方形遮罩）这两种，空白则是不需要遮罩。
    public string ClickType;//碰撞盒的大小  没实现
    public string ClickAction;//点击事件的调用
    public int NextID;//下一个引导的id
    public string Note;//备注 没实现
}

